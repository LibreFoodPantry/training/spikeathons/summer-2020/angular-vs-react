import React from 'react';

const lastName = "Radkowski";

function appendLastNameToFirstName(firstName) {
  return firstName + " " + lastName;
}


function Welcome(props) {
  return <h1>Hello, {appendLastNameToFirstName(props.name)}!</h1>;
}


function App() {
  return (
    <Welcome name="Chris" />
  );
}

export default App;
