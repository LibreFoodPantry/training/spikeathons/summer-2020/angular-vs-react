# Learning React App

## Description

This application was built to learn and follow along with various React tutorials.

## Prerequisites

- [Node.js](https://nodejs.org/en/)

## Running

To run the `learning-react-app` use the following instructions:

1. Open a terminal at the top-level for this application located at: ``angular-vs-react/learning-react-app``.
2. Run ``npm install``.
3. Run ``npm start``.
**Note this process will continue to run in the terminal until terminated using ``ctrl+c``.**
4. Open a web browser and navigate to ``localhost:3000`` to view the interface.

## Resources

- See [React's Getting Started](https://reactjs.org/docs/getting-started.html) page for more information about beginning with React.
- [React's Main Concepts](https://reactjs.org/docs/hello-world.html) was used for an introduction to React.
- [React's Creation guide](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app) was used to create this React app.


---
Copyright &copy; 2020 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.