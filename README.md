# Angular vs. React

## Description

This spike project explores learning how to use React. It also compares using React to Angular for implementing the same minimal frontend.

See https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/issue-board/-/issues/7 for more info. 

## Projects

- [learning-react-app](/learning-react-app) - A simple React app used to follow along with the various tutorials and guides for learning React.


---
Copyright &copy; 2020 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.